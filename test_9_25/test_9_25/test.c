#define _CRT_SECURE_NO_WARNINGS 1


#include <stdio.h>
//
//int main()
//{
//	printf("%s\n", __FILE__);
//	printf("%d\n", __LINE__);
//	printf("%s\n", __DATE__);
//	printf("%s\n", __TIME__);
//
//	//printf("%d\n", __STDC__);//未定义，VS是不支持ANSI C
//
//
//	return 0;
//}

//#define M 100;
//#define STR "hehe"
//
////for循环的判断部分如果省略后，判断条件就恒为真，这个循环就是死循环
//#define forever for( ; ; )
//
//
//int main()
//{
//	int a = M;
//	printf("%d\n", M);
//	printf("%s\n", STR);
//	forever;
//
//	return 0;
//}

//#define SQUARE(X) X*X
//
//int main()
//{
//	int a = 5;
//	printf("%d\n", SQUARE(a + 2));
//
//	return 0;
//}


//宏 - 1
//#define MAX(a, b) ((a)>(b)?(a):(b))
//
////函数 - 2
//int max(int x, int y)
//{
//	return x > y ? x : y;
//}
//
//
//int main()
//{
//	int n = MAX(100, 101);
//	//int n = ((100) > (101) ? (100) : (101));
//
//	int m = max(100, 101);
//	int r = max(100, 101);
//
//
//	//int q = MAX(100, 101);
//	//int q = ((100) > (101) ? (100) : (101));
//
//	return 0;
//}


//#define MALLOC(n, type)  (type*)malloc(n*sizeof(type))
//
//
//int main()
//{
//	//int *p = (int*)malloc(10*sizeof(int));
//	//malloc(10, int);
//	int* p = MALLOC(10, int);
//	//int *p = (int*)malloc(10*sizeof(int));
//
//	return 0;
//}

//void Print(int n)
//{
//	printf("the value of n is %d\n", n);
//}

//#define Print(n, format) printf("the value of " #n " is "format"\n", n)
//                                              //"a"
//                                              //"b"
//int main()
//{
//	/*printf("hello world\n");
//	printf("hello " "world\n");*/
//
//	int a = 10;
//	//printf("the value of a is %d\n", a);
//	Print(a, "%d");
//
//	int b = 20;
//	//printf("the value of b is %d\n", b);
//	Print(b, "%d");
//
//	float f = 3.14f;
//	//printf("the value of f is %f\n", f);
//	Print(f, "%f");
//
//	return 0;
//}
//
//#define M 100
//
//int main()
//{
//	int a = M;
//
//#undef M
//
//#define M 200
//	int b = M + 2;
//
//	return 0;
//}

//#define FLAG 3
//
//int main()
//{
//#if FLAG==2
//	printf("hehe\n");
//#endif
//
//	return 0;
//}

//#include"add.h"
#include <stddef.h>

struct S
{
	char c1;//0
	//3
	int i;  //4
	char c2;//8
};

#define OFFSETOF(type, mem) (size_t)&(((type*)0)->mem)


int main()
{
	printf("%d\n", OFFSETOF(struct S, c1));
	printf("%d\n", OFFSETOF(struct S, i));
	printf("%d\n", OFFSETOF(struct S, c2));

	return 0;
}

