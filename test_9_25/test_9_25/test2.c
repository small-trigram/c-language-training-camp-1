#include <stdio.h>

// int main()
// {
// 	printf("%s\n", __FILE__);
// 	printf("%d\n", __LINE__);
// 	printf("%s\n", __DATE__);
// 	printf("%s\n", __TIME__);

// 	printf("%d\n", __STDC__);//未定义，VS是不支持ANSI C
// 	return 0;
// }


// #define M 100;
// #define STR "hehe"

// //for循环的判断部分如果省略后，判断条件就恒为真，这个循环就是死循环
// #define forever for( ; ; )


// int main()
// {
// 	int a = M;
// 	printf("%d\n", M);
// 	printf("%s\n", STR);
// 	forever;

// 	return 0;
// }


// #define SQUARE(X) ((X)*(X))

// int main()
// {
// 	int a = 5;
// 	printf("%d\n", SQUARE(a+2));
// 	             //((a+2)*(a+2))
// 	return 0;
// }


// #define DOUBLE(x) ((x)+(x))

// int main()
// {
// 	int a = 4;
// 	printf("%d\n", 10*DOUBLE(a));
// 	               //10*((a)+(a))
// 	return 0;
// }


// int main()
// {
// 	int a = 10;
// 	int b = a+1;//b=11 a=10

// 	int a = 10;
// 	int b = ++a;//b=11 a=11 //带有副作用的

// 	return 0;
// }

//带有副作用的宏参数

//求2个整数的较大值

// #define MAX(a, b) ((a)>(b)?(a):(b))

// int max(int x, int y)
// {
// 	return x>y?x:y;
// }


// int main()
// {
// 	int a = 15;
// 	int b = 9;

// 	//int m = MAX(a++, b++);
// 	m  = max(a++, b++);

// 	//int m = ((a++)>(b++)?(a++):(b++));
// 	        //15 > 9      16
// 			             //17
// 	printf("%d\n", m);//16
// 	printf("a=%d b=%d\n", a, b);//17 10

// 	return 0;
// }


//宏 - 1
// #define MAX(a, b) ((a)>(b)?(a):(b))

// //函数 - 2
// int max(int x, int y)
// {
// 	return x>y?x:y;
// }

// #define M 100

// int main()
// {
// 	int m = MAX(M, 101);
// 	printf("M = %d\n", M);
	
// 	return 0;
// }

//GENERIC_MAX 通用的模板，这个模板可以定义函数

// #define GENERIC_MAX(type) \
// type type##_max(type x, type y)\
// {\
//        return x>y?x:y;\
// }

// //定义比较2个整型的函数
// GENERIC_MAX(int)

// //定义比较2个浮点型的函数
// GENERIC_MAX(float)


// GENERIC_MAX(char)

// int main()
// {
// 	int a = 10;
// 	int b = 20;
// 	int ret= int_max(a,b);
// 	printf("%d\n", ret);

// 	float f = float_max(3.5f, 4.1f);
// 	printf("%f\n", f);

// 	return 0;
// }




// int main()
// {
// 	int arr[SZ];
// 	int i = 0;
// 	for(i=0; i<SZ; i++)
// 	{
// 		arr[i] = i;
// 	}
// 	//打印
// 	for(i=0; i<SZ; i++)
// 	{
// 		printf("%d ", arr[i]);
// 	}

// 	return 0;
// }

// #define FLAG 2

// int main()
// {
// #if FLAG==2
// 	printf("hehe\n");
// #endif

// 	return 0;
// }

// #define FLAG 3

// int main()
// {
// #if FLAG==1
// 	printf("hehe\n");
// #elif FLAG==2
// 	printf("haha\n");
// #else
// 	printf("heihei\n");
// #endif
// 	return 0;
// }

//#define MAX 0

// int main()
// {
// // #if defined(MAX)
// // 	pritnf("hehe\n");
// // #endif

// #ifdef MAX
// 	pritnf("hehe\n");
// #endif

// 	return 0;
// }

//#define MAX 1
// int main()
// {
// #if !defined(MAX)
// 	pritnf("hehe\n");
// #endif

// // #ifndef MAX
// // 	pritnf("hehe\n");
// // #endif

// 	return 0;
// }

#include "add.h"
#include "add.h"
#include "add.h"
#include "add.h"
#include "add.h"

int main()
{
	
	return 0;
}