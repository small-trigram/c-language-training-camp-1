

#include <stdio.h>


//
//int main()
//{
//	//int num = 10;
//	//int* p = &num;
//
//	//char ch = 'w';
//	//char* pc = &ch;
//
//	//printf("%zd\n", sizeof(p));//
//	//printf("%zd\n", sizeof(pc));//
//
//	printf("%zd\n", sizeof(char*));
//	printf("%zd\n", sizeof(short*));
//	printf("%zd\n", sizeof(int*));
//	printf("%zd\n", sizeof(long*));
//	printf("%zd\n", sizeof(float*));
//	printf("%zd\n", sizeof(double*));
//
//	return 0;
//}


//int main()
//{
//	int n = 0x11223344;
//	int* p = &n;
//
//	*p = 0;
//
//	return 0;
//}

//int main()
//{
//	int n = 0x11223344;
//	char* p = &n;
//
//	*p = 0;
//
//	return 0;
//}


//int main()
//{
//	int n = 0x11223344;
//	int* p = &n;
//	char* pc = &n;
//
//	printf("p   = %p\n", p);
//	printf("p+1 = %p\n", p+1);
//
//	printf("pc   = %p\n", pc);
//	printf("pc+1 = %p\n", pc+1);
//
//
//	return 0;
//}

//
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	//            0 1 2 ...
//	// 
//	//指针访问访问
//	int i = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int* p = &arr[0];
//
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *(p+i));
//	}
//
//	/*for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *p);
//		p = p + 1;
//	}*/
//
//	//下标的方式
//	//int i = 0;
//	//int sz = sizeof(arr) / sizeof(arr[0]);
//	//for (i = 0; i < sz; i++)
//	//{
//	//	printf("%d ", arr[i]);
//	//}
//
//	return 0;
//}
//



//const 修饰变量，使得这个变量不能被修改
//const 修饰指针
//1. const放在*的左边
//   限制的是指针指向的内容，意思是不能通过指针来修改指针指向的内容，但是指针变量本身是可以修改的
//  
//2. const放在*的右边
//   限制的是指针变量本身，意思是不能修改指针变量的指向，但是可以修改指针指向的内容
// 
//int main()
//{
//	int m = 100;
//	const int n = 10;
//
//	const int * p = &n;
//
//	return 0;
//}
//
//int main()
//{
//	const int n = 100;
//	//n = 20;//err
//
//	const int * const p = &n;
//	*p = 20;
//
//	printf("%d\n", n);
//
//	return 0;
//}
//
//


//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	//使用指针
//	int* p = &arr[0];
//	int i = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *p);
//		p++;//p = p+1
//
//	}
//
//	/*for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *(p + i));
//	}*/
//	//
//	//p+i ---> &arr[i]
//	//
//	return 0;
//}


//int main()
//{
//	char arr[] = "abcdef";
//	//[a b c d e f \0]
//	printf("%s\n", arr);
//	char* pc = &arr[0];
//
//	while (*pc != '\0')
//	{
//		printf("%c", *pc);
//		pc++;
//	}
//
//	return 0;
//}




//指针-指针(地址-地址)的前提是两个指针指向同一块空间的
//得到的值的绝对值，是指针和指针之间元素的个数
//int* p +1 ---> 4

//int main()
//{
//	char ch[7] = {0};
//	int arr[10] = { 0 };
//
//	&arr[9] - &ch[5];
//	int ret = &arr[9] - &arr[0];
//	//
//	//&arr[0] + 9 ---> &arr[9]
//	//
//	printf("%d\n", ret);//?
//
//	return 0;
//}

#include <string.h>
//strlen - string length 
//功能：求字符串长度
//

//int my_strlen(char* str)
//{
//	int count = 0;
//	while (*str != '\0')
//	{
//		count++;
//		str++;
//	}
//	return count;
//}
//
//int main()
//{
//	char arr[] = "abcdef";
//	int len = my_strlen(arr);
//	printf("%d\n", len);
//	return 0;
//}


//int my_strlen(char* str)
//{
//	char* start = str;
//	while (*str != '\0')
//		str++;
//
//	return str - start;
//}
//
//int main()
//{
//	char arr[] = "abcdef";
//	int len = my_strlen(arr);
//	printf("%d\n", len);
//	return 0;
//}

//
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	//int* p = &arr[0];
//
//	int* p = arr;//数组名就是数组首元素的地址
//	while (p < arr + sz)//指针的关系运行
//	{
//		printf("%d ", *p);
//		p++;
//	}
//
//	return 0;
//}
//


//int main()
//{
//	int* p;//局部变量，如果不初始化的话，会得到一个随机值
//	*p = 20;
//
//
//	//int n = 10;
//	//int* p2 = &n;
//	//*p2 = 20;
//
//	return 0;
//}


//int main()
//{
//	int i = 0;
//	int arr[10] = { 0 };
//	int* p = &arr[0];
//
//	for (i = 0; i <= 11; i++)
//	{
//		*(p++) = i;
//		//*p
//		//p=p+1
//	}
//
//	return 0;
//}


//int* test()
//{
//	int n = 100;
//	//...
//	return &n;
//}
//
//int main()
//{
//	int *p = test();
//
//	return 0;
//}
//
