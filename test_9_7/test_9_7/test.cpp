#define _CRT_SECURE_NO_WARNINGS 1


#include <string.h>
#include <stdio.h>

//int main()
//{
//	//比较2个字符串
//	int ret = strcmp("abqe", "abq");
//	if (ret > 0)
//		printf(">\n");
//	else if (ret == 0)
//		printf("==\n");
//	else
//		printf("<\n");
//
//	printf("%d\n", ret);
//
//	return 0;
//}

#include <assert.h>

//int my_strcmp(const char* s1, const char* s2)
//{
//	assert(s1 != NULL);
//	assert(s2 != NULL);
//
//	while (*s1 == *s2)
//	{
//		if (*s1 == '\0')
//			return 0;
//
//		s1++;
//		s2++;
//	}
//	if (*s1 > *s2)
//		return 1;
//	else
//		return -1;
//}
//
//int my_strcmp(const char* s1, const char* s2)
//{
//	assert(s1 != NULL);
//	assert(s2 != NULL);
//
//	while (*s1 == *s2)
//	{
//		if (*s1 == '\0')
//			return 0;
//
//		s1++;
//		s2++;
//	}
//	return *s1 - *s2;
//}
//
//
//int main()
//{
//	//比较2个字符串
//	int ret = my_strcmp("ab", "abc");
//	//printf("%d\n", ret);
//	if (ret < 0)
//		printf("<\n");
//
//	return 0;
//}
//


//int main()
//{
//	char arr1[3] = { 0 };
//	char arr2[] = "abcdef";
//	strcpy(arr1, arr2);
//	printf("%s\n", arr1);
//
//	return 0;
//}


//int main()
//{
//	char arr1[10] = "xxxxxxxxx";
//	char arr2[] = "ab";
//	strncpy(arr1, arr2, 5);//大家下来要去思考，模拟实现strncpy?
//	printf("%s\n", arr1);
//
//	return 0;
//}


//int main()
//{
//	char arr1[20] = "abc\0xxxxxxxxx";
//	char arr2[] = "def";
//	strncat(arr1, arr2, 5);
//	printf("%s\n", arr1);
//
//	return 0;
//}


//int main()
//{
//	char arr1[20] = "abcdef";
//	char arr2[] = "abcqwer";
//	int ret = strncmp(arr1, arr2, 4);
//
//	printf("%d\n", ret);
//
//	return 0;
//}


//int main()
//{
//	char arr[] = "zpengwei@yeah.net";//@. 分割符
//	char buf[60] = { 0 };
//	//"zpengwei\0yeah\0net"
//	strcpy(buf, arr);
//	char* p = "@.";
//
//	char* r = NULL;
//	for (r = strtok(buf, p); r != NULL; r = strtok(NULL, p))
//	{
//		printf("%s\n", r);
//	}
//	
//	return 0;
//}
//
//const char* my_strstr(const char* str1, const char * str2)
//{
//	assert(str1);
//	assert(str2);
//
//	const char* cp = str1;
//	const char* s1 = NULL;
//	const char* s2 = NULL;
//
//	//如果子串是空字符串，直接返回str1
//	if (*str2 == '\0')
//		return str1;
//
//	while (*cp)
//	{
//		s1 = cp;
//		s2 = str2;
//		while (*s1 == *s2 && *s1 && *s2)
//		{
//			s1++;
//			s2++;
//		}
//
//		if (*s2 == '\0')
//			return cp;
//
//		cp++;
//	}
//
//	return NULL;
//}
//
//int main()
//{
//	char arr1[] = "abbbcdef";
//	char arr2[] = "bbc";
//	char* ret = my_strstr(arr1, arr2);
//	if (ret != NULL)
//		printf("%s\n", ret);
//	else
//		printf("找不到\n");
//
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		char * ret = strerror(i);
//		printf("%d : %s\n", i, ret);
//	}
//
//	return 0;
//}


//当库函数调用失败的时候，会讲错误码记录到errno这个变量中
//errno是一个C语言的全局变量

//打开文件 - 读写文件之前，需要打开文件
//读取文件前，需要打开文件，如果要打开成功，需要文件是存在的，如果文件不存在，则打开失败,fopen会返回NULL

//#include <string.h>
//#include <errno.h>
//
////相关的函数：perror
//int main()
//{
//	FILE* pf = fopen("add.txt", "r");
//	if (pf == NULL)
//	{
//		//printf("打开文件失败，失败的原因: %s\n", strerror(errno));
//		perror("fopen");
//		printf("fopen: %s\n", strerror(errno));
//		return 1;
//	}
//	else
//	{
//		printf("打开文件成功\n");
//		//...
//	}
//	return 0;
//}
//

//memcpy函数返回的是目标空间的起始地址


//不建议这样写
//void* my_memcpy(void* dest, const void* src, size_t num)
//{
//	void* ret = dest;
//	assert(dest && src);
//	while (num--)
//	{
//		//拷贝一个字节
//		*(char*)dest = *(char*)src;
//		++(char*)dest;
//		++(char*)src;//存在一定的风险
//	}
//	return ret;
//}


//
//void* my_memcpy(void* dest, const void* src, size_t num)
//{
//	void* ret = dest;
//	assert(dest && src);
//	while (num--)
//	{
//		//拷贝一个字节
//		*(char*)dest = *(char*)src;
//		dest = (char*)dest + 1;
//		src = (char*)src + 1;
//	}
//	return ret;
//}
//
//int main()
//{
//	int arr1[10] = { 0 };
//	int arr2[] = { 1,2,3,4,5,6,7,8 };
//	my_memcpy(arr1, arr2, 20);
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr1[i]);
//	}
//
//	//char arr3[] = "xxxxxxxxxx";
//	//char arr4[] = "yyyyyyyy";
//	//memcpy(arr3, arr4, 3);
//
//	return 0;
//}



void* my_memcpy(void* dest, const void* src, size_t num)
{
	void* ret = dest;
	assert(dest && src);
	while (num--)
	{
		//拷贝一个字节
		*(char*)dest = *(char*)src;
		dest = (char*)dest + 1;
		src = (char*)src + 1;
	}
	return ret;
}

int main()
{
	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
	//1 2 1 2 3 4 5 8 9 10
	//1 2 1 2 1 2 1 8 9 10
	memmove(arr+2, arr, 20);
	int i = 0;
	for (i = 0; i < 10; i++)
	{
		printf("%d ", arr[i]);
	}

	return 0;
}

