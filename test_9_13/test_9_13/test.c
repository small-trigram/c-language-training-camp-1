#define _CRT_SECURE_NO_WARNINGS 1


#include <stdio.h>
//X64 - 64位环境下，指针的大小是8个字节
// 
//X86 - 32位环境下，指针的大小是4个字节
//

//int main()
//{
//    int a[4] = { 1, 2, 3, 4 };
//    int* ptr1 = (int*)(&a + 1);
//    //a是数组名，是数组首元素的地址 - 是地址，是地址是64位环境下就是8个字节
//    //a - 0x0012ffcd30542320
//    //0x30542320
//    int* ptr2 = (int*)((int)a + 1);
//
//    printf("%x,%x", ptr1[-1], *ptr2);
//    return 0;
//}
//



#include <stdio.h>
//
//int main()
//{
//    int n = 9;//整型的存储方式，补码
//    //00000000000000000000000000001001   9的补码
//    //
//    float* pFloat = (float*)&n;
//
//    printf("n的值为：%d\n", n);//9
//    printf("*pFloat的值为：%f\n", *pFloat);
//    //0 00000000 00000000000000000001001
//    //E 为全0
//    //(-1)^0 * 0.00000000000000000001001 *  2^-126
//    //1*
//    //0.000000
//
//    *pFloat = 9.0;
//    //1001.0
//    //(-1)^0 * 1.001*2^3
//    //S=0
//    //M=1.001
//    //E = 3
//    //01000001000100000000000000000000
//    //
//    printf("num的值为：%d\n", n);
//    printf("*pFloat的值为：%f\n", *pFloat);//9.0
//    return 0;
//}





//int main()
//{
//	float f = 5.5;
//	//0 10000001 01100000000000000000000
//	//(-1)^0 * 1.011 * 2^2
//	//0x40 b0 00 00
//	//
//	return 0;
//}


//struct Stu
//{
//	char name[20];
//	int age;
//	float score;
//} s4, s5;//全局变量
//
//struct Stu
//{
//	char name[20];
//	int age;
//	float score;
//}s3 = {"wangwu", 24, 98.0f};
//
//
//int main()
//{
//	struct Stu s1 = {"zhangsan", 20, 98.5f};
//	struct Stu s2 = { "lisi", 33, 68.5f };
//	struct Stu s4 = { .age = 22, .name = "ruhua", .score = 55.5f };
//
//	printf("%s %d %f\n", s1.name, s1.age, s1.score);
//	printf("%s %d %f\n", s4.name, s4.age, s4.score);
//
//	return 0;
//}
//
//
//struct
//{
//	char a;
//	int c;
//	float d;
//}s = {0};
//
//struct
//{
//	char a;
//	int c;
//	float d;
//}* ps;
//
//int main()
//{
//	ps = &s;//err - 虽然结构类型的成员一模一样，但是编译器依然认为=的两边是不同的指针类型
//
//	return 0;
//}


//struct Node
//{
//	int data;//存数据 - 4
//	struct Node next;
//};

//struct Node
//{
//	int data;//存数据 - 4
//	struct Node* next;//下一个节点的地址
//};

//
//typedef struct Node
//{
//	int data;//存数据 - 4
//	struct Node * next;//下一个节点的地址
//}Node;
//
//
//
//int main()
//{
//	Node n = {0};
//
//	return 0;
//}




//
//struct S1
//{
//	char c1;
//	char c2;
//	int a;
//};
//
//struct S2
//{
//	char c1;
//	int a;
//	char c2;
//};
//
//
//int main()
//{
//	struct S2 s2 = { 'a', 100, 'b'};
//	printf("%zd\n", sizeof(s2));
//
//	struct S1 s1 = { 'a', 'b', 100 };
//	printf("%zd\n", sizeof(s1));
//	return 0;
//}
//

#include <stddef.h>
//
//struct S3
//{
//    double d;
//    char c;
//    int i;
//};
//
//struct S4
//{
//    char c1;
//    struct S3 s3;
//    double d;
//};
//
//int main()
//{
//    //printf("%zd\n", sizeof(struct S3));//?
//    //printf("%zd\n", sizeof(struct S4));//?
//    //offsetof();//宏 - 用来计算结构体成员，相较于起始位置的偏移量的
//
//    printf("%zd\n", offsetof(struct S4, c1));
//    printf("%zd\n", offsetof(struct S4, s3));
//    printf("%zd\n", offsetof(struct S4, d));
//
//	return 0;
//}


#include <stdio.h>

//#pragma pack(1)//设置默认对齐数为1
//struct S
//{
//    char c1;
//    int i;
//    char c2;
//};
//#pragma pack()//取消设置的对齐数，还原为默认
//
//int main()
//{
//    //输出的结果是什么？
//    printf("%zd\n", sizeof(struct S));
//    return 0;
//}

struct S
{
    int data[1000];
    int num;
};

void print1(struct S t)
{
    int i = 0;
    for(i=0; i<10; i++)
    {
        printf("%d ", t.data[i]);
    }
    printf("\n");
    printf("num = %d\n", t.num);
}

void print2(const struct S * ps)
{
    int i = 0;
    for (i = 0; i < 10; i++)
    {
        printf("%d ", ps->data[i]);
    }
    printf("\n");
    printf("num = %d\n", ps->num);
}

int main()
{
    struct S s = { {1,2,3,4}, 1000 };

    print1(s);//传递结构体变量 - 传值调用
    print2(&s);//传递结构体变量的地址 - 传址调用

    return 0;
}

