#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <string.h>
#include <assert.h>
//
//void* my_memcpy(void* dest, const void* src, size_t num)
//{
//	void* ret = dest;
//	assert(dest);
//	assert(src);
//	while (num--)
//	{
//		*(char*)dest = *(char*)src;
//		dest = (char*)dest + 1;
//		src = (char*)src + 1;
//	}
//	return ret;
//}
//
////这个函数最终会返回目标空间的起始地址
//void* my_memmove(void* dest, const void* src, size_t num)
//{
//	void* ret = dest;
//	assert(dest);
//	assert(src);
//
//	//分情况讨论
//	if (dest < src)
//	{
//		//前->后
//		while (num--)
//		{
//			*(char*)dest = *(char*)src;
//			dest = (char*)dest + 1;
//			src = (char*)src + 1;
//		}
//	}
//	else
//	{
//		//后->前
//		while (num--)
//		{
//			//num=18
//			*((char*)dest + num) = *((char*)src + num);
//		}
//	}
//	return ret;
//}
//
//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//	//my_memmove(arr1, arr1+2, 20);
//	//my_memcpy(arr1+2, arr1, 20);
//	memcpy(arr1 + 2, arr1, 20);
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", arr1[i]);
//	}
//
//	return 0;
//}


//int main()
//{
//	char arr[] = "hello bit";
//	//xxxxx bit
//	//hello xxx
//
//	//memset(arr, 'x', 5);
//	memset(arr+6, 'x', 3);
//	printf("%s\n", arr);
//
//	return 0;
//}

//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	//注意点：memset是以字节为单位设置内存值的
//	memset(arr, 0, 20);
//
//	return 0;
//}

//int main()
//{
//	char arr1[] = "abcdef";
//	char arr2[] = "abqwertyuiop";
//	int ret = memcmp(arr1, arr2, 2);
//	printf("%d\n", ret);
//
//	return 0;
//}
//
//int main()
//{
//	int arr1[] = { 1,0x01234503,3,4,5 };
//	int arr2[] = { 1,3,5,7,9 };
//	int ret = memcmp(arr1, arr2, 5);
//	printf("%d\n", ret);
//
//	return 0;
//}


//int main()
//{
//	int a = 5;
//	//00000000000000000000000000000101 5的原码
//	//00000000000000000000000000000101 5的反码
//	//00000000000000000000000000000101 5的补码
//
//	int b = -5;
//	//10000000000000000000000000000101 -5的原码
//	//11111111111111111111111111111010 -5的反码
//	//11111111111111111111111111111011 -5的补码
//
//	return 0;
//}


//int main()
//{
//	int a = 11;
//	//00000000 00000000 00000000 00001011
//	//0x 00 00 00 0b
//	int b = 0x11223344;
//
//	return 0;
//}
//

//设计一个小程序来判断当前机器的字节序

//int check_sys()
//{
//	int a = 1;
//	if ((*(char*)&a) == 1)
//	{
//		return 1;//小端
//	}
//	else
//	{
//		return 0;//大端
//	}
//}

//
//int check_sys()
//{
//	int a = 1;
//	if ((*(char*)&a) == 1)
//	{
//		return 1;//小端
//	}
//	else
//	{
//		return 0;//大端
//	}
//}


//int check_sys()
//{
//	int a = 1;
//	return (*(char*)&a);//小端返回1，大端返回0
//}
//
//int main()
//{
//	if (check_sys() == 1)
//	{
//		printf("小端\n");
//	}
//	else
//	{
//		printf("大端\n");
//	}
//	return 0;
//}

//int main()
//{
//	if (check_sys() == 1)
//	{
//		printf("小端\n");
//	}
//	else
//	{
//		printf("大端\n");
//	}
//	return 0;
//}
//
//#include <stdio.h>
//int main()
//{
//    char a = -1;
//    //10000000000000000000000000000001
//    //11111111111111111111111111111110
//    //11111111111111111111111111111111
//    //截断
//    //11111111 - a
//    //11111111111111111111111111111111 - 补码
//    //10000000000000000000000000000000
//    //10000000000000000000000000000001-> -1
//    signed char b = -1;
//    //10000000000000000000000000000001
//    //11111111111111111111111111111110
//    //11111111111111111111111111111111
//    //11111111 - b
//    //11111111111111111111111111111111
//    //10000000000000000000000000000000
//    //10000000000000000000000000000001-> -1
//    unsigned char c = -1;
//    //10000000000000000000000000000001
//    //11111111111111111111111111111110
//    //11111111111111111111111111111111
//    //11111111 - c
//    //00000000000000000000000011111111
//    //00000000000000000000000011111111 - 255
//    printf("a=%d,b=%d,c=%d", a, b, c);
//    //%d - 以十进制的形式打印有符号的整型
//    //-1
//    
//    return 0;
//}
//

#include <stdio.h>

//int main()
//{
//    char a = -128;
//    //10000000000000000000000010000000
//    //11111111111111111111111101111111
//    //11111111111111111111111110000000
//    //10000000 - a
//    //11111111111111111111111110000000
//    //10000000000000000000000001111111
//    //10000000000000000000000010000000
//    //-128
//    printf("%d\n", a);//%d 打印有符号整数
//    printf("%u\n", a);//%u 打印无符号整数
//    //
//    return 0;
//}
//

//#include <stdio.h>
//int main()
//{
//    char a = 128;
//    //00000000000000000000000010000000
//    //10000000 -a
//
//    printf("%u\n", a);
//    return 0;
//}


#include <stdio.h>

//int main()
//{
//    char a[1000];
//    int i;
//    for (i = 0; i < 1000; i++)
//    {
//        a[i] = -1 - i;
//    }
//    printf("%d", strlen(a));
//    return 0;
//}
//


//#include <stdio.h>
//
//unsigned char i = 0;//0~255
//
//int main()
//{
//    //死循环
//    for (i = 0; i <= 255; i++)
//    {
//        printf("hello world\n");
//    }
//
//    return 0;
//}


//#include <stdio.h>
//
//#include <windows.h>
//
//int main()
//{
//    unsigned int i;//i>=0
//    for (i = 9; i >= 0; i--)
//    {
//        printf("%u\n", i);
//        Sleep(1000);
//    }
//    return 0;
//}
//
//


#include <stdio.h>

int main()
{
    int a[4] = { 1, 2, 3, 4 };

    int* ptr1 = (int*)(&a + 1);
    int* ptr2 = (int*)((long long)a + 1);
    
    printf("%x,%x", ptr1[-1], *ptr2);
    return 0;
}






























