#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>

//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	//            0 1 2 3 4 5 6 7 8 9
//	int k = 0;
//	scanf("%d", &k);//7
//	int i = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int find = 0;//假设找不到
//	for (i = 0; i < sz; i++)
//	{
//		if (k == arr[i])
//		{
//			printf("找到了，下标是%d\n", i);
//			find = 1;
//			break;
//		}
//	}
//	if (find == 0)
//	{
//		printf("找不到\n");
//	}
//
//	return 0;
//}
//
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int k = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	scanf("%d", &k);
//	//查找-二分查找
//	int left = 0;
//	int right = sz - 1;
//	int find = 0;//假设找不到
//
//	while (left<=right)
//	{
//		int mid = left + (right-left)/2;
//		if (arr[mid] < k)
//		{
//			left = mid + 1;
//		}
//		else if (arr[mid] > k)
//		{
//			right = mid - 1;
//		}
//		else
//		{
//			printf("找到了，下标是%d\n", mid);
//			find = 1;
//			break;
//		}
//	}
//	if (find == 0)
//	{
//		printf("找不到了\n");
//	}
//	return 0;
//}

#include <limits.h>

//int main()
//{
//	//INT_MAX;
//
//	int left = 2147483646;
//	int right = 2147483646;
//
//	int mid = left+(right-left)/2;
//
//	printf("%d\n", mid);
//
//	return 0;
//}

#include <math.h>

//int main()
//{
//	double r = sqrt(16.0);
//	printf("%lf\n", r);
//
//	return 0;
//}
//



//#include <math.h>
//
//int main()
//{
//	double r = pow(7.0, 3.0);
//	printf("%lf\n", r);
//
//	return 0;
//}

//函数的返回类型有2类：
//1. void - 表示什么都不返回
//2. 其他类型 int char short ...
//
//void menu(void)
//{
//	printf("***********************\n");
//	printf("***********************\n");
//	printf("***********************\n");
//}
//int main()
//{
//	return 0;
//}
//
//


//int Add(int x, int y)
//{
//	int z = 0;
//	z = x + y;
//	return z;
//}

//int Add(int x, int y)//形式上的参数，简称形参
//{
//	return x + y;
//}
//
//int main()
//{
//	int a = 0;
//	int b = 0;
//	//输入
//	scanf("%d %d", &a, &b);
//	//计算求和
//	int c = Add(a, b);//a和b是真实传递给Add的参数，是实际参数，简称实参
//	//输出
//	printf("%d\n", c);
//
//	return 0;
//}


//void test()
//{
//	int n = 0;
//	scanf("%d", &n);
//	printf("hehe\n");
//	if (n == 5)
//		return;
//	printf("haha\n");
//}
//
//int main()
//{
//	test();
//	return 0;
//}
//

//void test()
//{
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		if (i == 5)
//			return;
//		printf("hehe\n");
//	}
//	//
//	printf("haha\n");
//}
//
//int main()
//{
//	test();
//	return 0;
//}

//int test()
//{
//	return 3.14;
//}
//
//int main()
//{
//	int n = test();
//	printf("%d\n", n);
//
//	return 0;
//}

//int test()
//{
//	int n = 0;
//	if (n == 5)
//		return 1;
//	else
//		return - 1;
//}
//
//int main()
//{
//	int m = test();
//	printf("%d\n", m);
//
//	return 0;
//}
//
//
//void set_arr(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		arr[i] = -1;
//	}
//}
//
//void print_arr(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//}
//
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	print_arr(arr, sz);
//
//	//先写一个函数，将arr中的内容全部设置为-1
//	set_arr(arr, sz);
//	//写一个函数，将arr中的内容打印出来
//	print_arr(arr, sz);
//
//	return 0;
//}


//void print_arr(int arr[3][5], int r, int c)
//{
//	int i = 0;
//	for (i = 0; i < r; i++)
//	{
//		int j = 0;
//		for (j = 0; j < c; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//}
//
//int main()
//{
//	int arr[3][5] = { 1,2,3,4,5, 2,3,4,5,6, 3,4,5,6,7 };
//	print_arr(arr, 3, 5);
//
//	return 0;
//}

//如果是闰年，返回1
//如果不是闰年，返回0
//判断y是否闰年
//
//int is_leap_year(int y)
//{
//	if ((y % 4 == 0 && y % 100 != 0) || (y % 400 == 0))
//		return 1;
//	else
//		return 0;
//}
//
////获取某年某月有多少天
//int get_days_of_month(int y, int m)//1990 3
//{
//	int days[13] = { 0, 31,28,31,30,31,30,31,31,30,31,30,31 };
//		//           0  1  2  3  4  5
//	int d = days[m];
//	if (is_leap_year(y) && m == 2)
//	{
//		d += 1;
//	}
//	return d;
//}
//
//int main()
//{
//	int y = 0;//年
//	int m = 0;//月
//	scanf("%d %d", &y, &m);//1990 3
//	int d = get_days_of_month(y, m);
//	printf("%d\n", d);
//	return 0;
//}


//函数是可以嵌套调用的，但是不能嵌套定义

#include <string.h>
//
//int main()
//{
//	size_t len = strlen("abc");//a b c \0
//	printf("%zd\n", len);
//
//	return 0;
//}



//int main()
//{
//	printf("%zd\n", strlen("abc"));
//
//	return 0;
//}


#include <stdio.h>

int main()
{
	printf("%d", printf("%d", printf("%d", 43)));

	//43
	return 0;
}


