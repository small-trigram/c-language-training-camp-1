#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

//struct A
//{
//    int _a : 2;//只占2个二进制位
//    int _b : 5;//只占5个二进制位
//    int _c : 10;//只占10个二进制位
//    int _d : 30;//只占30个二进制位
//};//8
//
//struct B
//{
//    int _a;//0 1 2 3
//    //00
//    //01
//    //10
//    //11
//    int _b;
//    int _c;
//    int _d;
//};//16
//
//int main()
//{
//    printf("%d\n", sizeof(struct A));
//    printf("%d\n", sizeof(struct B));
//
//	return 0;
//}



//struct S
//{
//    char a : 3;
//    char b : 4;
//    char c : 5;
//    char d : 4;
//};
//
//int main()
//{
//    struct S s = { 0 };
//    s.a = 10;
//    s.b = 12;
//    s.c = 3;
//    s.d = 4;
//
//    printf("%d\n", sizeof(struct S));
//
//    return 0;
//}
//

//
//struct A
//{
//    int _a : 2;
//    int _b : 5;
//    int _c : 10;
//    int _d : 30;
//};
//
//
//int main()
//{
//    struct A sa = { 0 };
//    //scanf("%d", &(sa._b));//这是错误的
//
//    //正确的示范
//    int b = 0;
//    scanf("%d", &b);
//    sa._b = b;
//    return 0;
//}

//结构体
struct S
{
	char c;
	int i;
};

//联合体/共用体
//union U
//{
//	char c;
//	int i;
//};
//
//int main()
//{
//	union U u = {0};
//	printf("%d\n", sizeof(u));
//
//	printf("%p\n", &u);
//	printf("%p\n", &(u.i));
//	printf("%p\n", &(u.c));
//
//
//	return 0;
//}

#include <stdio.h>

//联合类型的声明
//union Un
//{
//    char c;
//    int i;
//};
//
//int main()
//{
//    //联合变量的定义
//    union Un un = { 0 };
//    un.i = 0x11223344;
//    un.c = 0x55;
//    printf("%x\n", un.i);
//    return 0;
//}

#include <stdio.h>

//union Un1
//{
//    char c[5];//5   1 8 1
//    int i;//4       4 8 4
//};
//
//union Un2
//{
//    short c[7];//14   2   8  2
//    int i;//4         4   8  4
//};
//
//int main()
//{
//    //下面输出的结果是什么？
//    printf("%d\n", sizeof(union Un1));//8
//    printf("%d\n", sizeof(union Un2));//16
//    return 0;
//}

//联合体的大小，是其中最大成员的大小 (这句话是错误的)
//
//struct gift_list
//{
//    int stock_number;//库存量
//    double price; //定价
//    int item_type;//商品类型
//
//    union {
//        struct
//        {
//            char title[20];//书名
//            char author[20];//作者
//            int num_pages;//页数
//        }book;
//        struct
//        {
//            char design[30];//设计
//        }mug;
//        struct
//        {
//            char design[30];//设计
//            int colors;//颜色
//            int sizes;//尺寸
//        }shirt;
//    }item;
//};
//
//int main()
//{
//    struct gift_list gl = {0};
//    gl.stock_number = 100;
//    gl.price = 25;
//    gl.item_type = 2;
//
//    return 0;
//}

//写一个程序，判断当前机器是大端？还是小端？

//int main()
//{
//	int a = 1;
//	
//	if (*(char*)&a == 1)
//	{
//		printf("小端\n");
//	}
//	else
//	{
//		printf("大端\n");
//	}
//
//	return 0;
//}
//
//int check_sys()
//{
//	union
//	{
//		char c;
//		int i;
//	}u;
//	u.i = 1;
//	return u.c;
//}
//
//int main()
//{
//	
//	if (check_sys() == 1)
//		printf("小端\n");
//	else
//		printf("大端\n");
//
//	return 0;
//}
//

//
//enum Day
//{
//    //列出的是枚举类型的可能取值
//    //这些列出的可能取值被称为：枚举常量
//    Mon,//0
//    Tues,//1
//    Wed,//2
//    Thur,//3
//    Fri,
//    Sat,
//    Sun
//};
//
//enum Sex
//{
//    MALE=4,//0
//    FEMALE=7,//1
//    SECRET=1//2
//};
//
//int main()
//{
//    //printf("%d %d %d %d %d %d %d\n", Mon,Tues,Wed,Thur,Fri,Sat,Sun);
//    printf("%d %d %d\n", MALE, FEMALE, SECRET);
//    //MALE = 4;//ERR
//    enum Sex s = MALE;
//    enum Day d = Sun;
//
//    return 0;
//}


//
//void menu()
//{
//	printf("*********************\n");
//	printf("*** 1.add  2.sub  ***\n");
//	printf("*** 3.mul  4.div  ***\n");
//	printf("*** 0.exit        ***\n");
//	printf("*********************\n");
//}
//
//enum Option
//{
//	EXIT,
//	ADD,
//	SUB,
//	MUL,
//	DIV
//};
//
////#define EXIT 0
////#define ADD 1
////#define SUB 2
////
////#define XOR 5
//
//int main()
//{
//	int input = 0;
//	do
//	{
//		menu();
//		printf("请选择:");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case ADD:
//
//			break;
//		case MUL:
//
//			break;
//		default:
//			break;
//		}
//	} while (input);
//
//	return 0;
//}
//
//enum Color//颜色
//{
//    RED = 1,
//    GREEN = 2,
//    BLUE = 4
//};
//
//int main()
//{
//
//    enum Color clr = GREEN;//使用枚举常量给枚举变量赋值
//    enum Color clr2 = 2;
//    printf("%d\n", sizeof(clr));//4
//    return 0;
//}


//int main()
//{
//	int a = 10;//申请4个字节
//	char c = 'w';//申请1个字节
//	int arr[30] = { 0 };//申请120节，但是申请好之后，空间大小不能调整
//	//变长数组只是说数组的大小可以使用变量来指定，而一旦数组创建好后，依然是不能调整大小
//	//申请的空间大小不能灵活的调整
//	//C语言就给了：动态内存管理，给程序员权限，自己申请，自己使用，使用完后，自己释放
//
//	return 0;
//}
#include <stdlib.h>

int main()
{
	//申请10个整型的空间 - 40个字节
	int *p = (int*)malloc(10 * sizeof(int));

	if (p == NULL)
	{
		perror("malloc");
		return 1;
	}
	//使用空间
	int i = 0;
	for (i = 0; i < 10; i++)
	{
		*(p + i) = i;
	}
	//0 1 2 3 4 5 6 7 8 9
	for (i = 0; i < 10; i++)
	{
		printf("%d ", p[i]);
	}

	//释放
	free(p);
	p = NULL;

	return 0;
}

