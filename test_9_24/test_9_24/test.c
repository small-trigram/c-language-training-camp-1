#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>

//int main()
//{
//	FILE* pf = fopen("data.txt", "w");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	//写文件
//	fputc('q', pf);
//
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

//int main()
//{
//	fputc('b', stdout);
//	fputc('i', stdout);
//	fputc('t', stdout);
//
//	return 0;
//}

//使用fgetc从标准输入流中读取数据

//int main()
//{
//	//int ch = fgetc(stdin);
//	int ch = getchar();
//	printf("%c\n", ch);
//
//
//	return 0;
//}

//
//int main()
//{
//	FILE* pf = fopen("data.txt", "r");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	//读文件
//	int ch = fgetc(pf);
//	printf("%c\n", ch);
//
//	ch = fgetc(pf);
//	printf("%c\n", ch);
//
//	ch = fgetc(pf);
//	printf("%c\n", ch);
//
//	ch = fgetc(pf);
//	printf("%d\n", ch);
//
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

//文件的拷贝
//
//int main()
//{
//	FILE* pfread = fopen("data1.txt", "r");
//	if (pfread == NULL)
//	{
//		perror("fopen-1");
//		return 1;
//	}
//	
//	FILE * pfwrite = fopen("data2.txt", "w");
//	if (pfwrite == NULL)
//	{
//		perror("fopen-2");
//		fclose(pfread);
//		pfread = NULL;
//		return 1;
//	}
//
//	//读文件 - 写文件
//	int ch = 0;
//
//	while ((ch = fgetc(pfread)) != EOF)
//	{
//		fputc(ch, pfwrite);
//	}
//
//	//关闭文件
//	fclose(pfread);
//	pfread = NULL;
//	fclose(pfwrite);
//	pfwrite = NULL;
//
//	return 0;
//}
//


//

//
//int main()
//{
//	FILE* pf = fopen("data.txt", "w");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	//写一行
//	fputs("hello world\n", pf);
//	fputs("hehe\n", pf);
//
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}


//
//int main()
//{
//	FILE* pf = fopen("data.txt", "r");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	char arr[20] = "xxxxxxxxxxxxxxxxx";
//	fgets(arr, 20, pf);
//	printf("%s", arr);
//
//	fgets(arr, 20, pf);
//	printf("%s", arr);
//
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}


//
//struct S
//{
//	int n;
//	float f;
//	char arr[20];
//};
//
//int main()
//{
//	struct S s = {100, 3.14f, "zhangsan"};
//
//	FILE* pf = fopen("data.txt", "w");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	//写文件
//	fprintf(pf, "%d %f %s", s.n, s.f, s.arr);
//
//	fclose(pf);
//	pf = NULL;
//
//	return 0;
//}





//
//struct S
//{
//	int n;
//	float f;
//	char arr[20];
//};
//
//int main()
//{
//	struct S s = {0};
//
//	FILE* pf = fopen("data.txt", "r");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	//读文件
//	fscanf(pf, "%d %f %s", &(s.n), &(s.f), s.arr);
//	printf("%d %f %s\n", s.n, s.f, s.arr);
//
//	fclose(pf);
//	pf = NULL;
//
//	return 0;
//}


//
//struct S
//{
//	int n;
//	float f;
//	char arr[20];
//};
//
//int main()
//{
//	struct S s = { 100, 3.14f, "zhangsan"};
//	char arr[30] = { 0 };
//	sprintf(arr, "%d %f %s", s.n, s.f, s.arr);
//	printf("%s\n", arr);
//
//	//从arr 这个字符串中提取出格式化的数据
//	struct S t = { 0 };
//	sscanf(arr, "%d %f %s", &(t.n), &(t.f), t.arr);
//	printf("%d %f %s\n", t.n, t.f, t.arr);
//
//	
//	return 0;
//}
//


//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7 };
//	FILE* pf = fopen("data.txt", "wb");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	fwrite(arr, sizeof(int), 7, pf);
//
//	fclose(pf);
//	pf = NULL;
//
//	return 0;
//}
//
//int main()
//{
//	int arr[10] = {0};
//
//	FILE* pf = fopen("data.txt", "rb");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	
//	fread(arr, sizeof(int), 7, pf);
//
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d\n", arr[i]);
//	}
//
//	fclose(pf);
//	pf = NULL;
//
//	return 0;
//}



//
//int main()
//{
//	int arr[10] = { 0 };
//
//	FILE* pf = fopen("data.txt", "r");
//	if (pf == NULL)
//	{
//		perror("fopen");
//		return 1;
//	}
//	//读文件
//	//定位文件指针
//	//fseek(pf, 6,SEEK_SET);
//	//fseek(pf, -3, SEEK_END);
//
//	int ch = fgetc(pf);
//	printf("%c\n", ch);//a
//
//	fseek(pf, 5, SEEK_CUR);
//
//	ch = fgetc(pf);
//	printf("%c\n", ch);//g
//
//	int pos = ftell(pf);
//	printf("%d\n", pos);
//
//	rewind(pf);
//	
//	ch = fgetc(pf);
//	printf("%c\n", ch);//a
//
//
//	fclose(pf);
//	pf = NULL;
//
//	return 0;
//}
//
//#include <stdio.h>
//#include <windows.h>
////VS2022/11 WIN11环境测试
//
//int main()
//{
//    FILE* pf = fopen("test.txt", "w");
//
//    fputs("abcdef", pf);//先将代码放在输出缓冲区
//    printf("睡眠10秒-已经写数据了，打开test.txt文件，发现文件没有内容\n");
//    Sleep(10000);
//    printf("刷新缓冲区\n");
//    fflush(pf);//刷新缓冲区时，才将输出缓冲区的数据写到文件（磁盘）
//    //注：fflush 在高版本的VS上不能使用了
//    printf("再睡眠10秒-此时，再次打开test.txt文件，文件有内容了\n");
//    Sleep(10000);
//    fclose(pf);
//    //注：fclose在关闭文件的时候，也会刷新缓冲区
//    pf = NULL;
//
//    return 0;
//}

extern int Add(int x, int y);
extern int g_val;

int main() 
{
	printf("%d\n", g_val);
	printf("%d\n", Add(2, 3));

	return 0;
}

