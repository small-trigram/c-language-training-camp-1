#define _CRT_SECURE_NO_WARNINGS 1

#include <ctype.h>
#include <stdio.h>
#include <string.h>


//islower 是用来判断参数是否是小写字母的
//如果是小写字母返回非0的值
//如果不是小写字母返回0

//int main()
//{
//	int ret = islower('Q');
//	printf("%d\n", ret);
//	return 0;
//}

//int main()
//{
//	int ret = isupper('q');
//	printf("%d\n", ret);
//	return 0;
//}

//int main()
//{
//	int ret = isdigit('w');
//	printf("%d\n", ret);
//	return 0;
//}


//int main()
//{
//	int ch = 0;
//	ch = getchar();
//	//if (ch >= 'a' && ch <= 'z')
//	if(islower(ch))
//	{
//		//ch是小写字母
//
//	}
//
//	return 0;
//}

//写一个代码，将字符串中的小写字母转大写，其他字符不变。

//'a' 97
//'A' 65
//'b' 98
//'B' 66
//int main()
//{
//	char str[] = "i am A Student";
//	//            0123
//	//1. 遍历字符串
//	//2. 发现小写字母转大写
//	size_t len = strlen(str);//14(0~13)
//	size_t i = 0;
//	for (i = 0; i < len; i++)
//	{
//		if (islower(str[i]))
//		{
//			//转大写
//			//str[i] = str[i] - 32;
//			str[i] = toupper(str[i]);
//		}
//	}
//	printf("%s\n", str);
//
//	return 0;
//}

//int main()
//{
//	char ch = 0;
//	scanf("%c", &ch);//A
//	//ch = ch - 32;
//	//ch = toupper(ch);
//	ch = tolower(ch);
//	printf("%c\n", ch);
//
//	return 0;
//}

//
//int main()
//{
//	//strlen - 求字符串长度的
//	//字符串的结束标志是\0
//	//strlen统计的是\0之前出现的字符的个数
//	//基本功能
//	//char arr[] = "abcdef";
//	////a b c d e f \0
//	//size_t len = strlen(arr);
//	//printf("%zd\n", len);
//
//	//1. strlen函数要正确获得字符串长度的话，字符串中必须得有\0
//	/*char arr[] = { 'a', 'b', 'c' , '\0'};
//	size_t len = strlen(arr);
//	printf("%zd\n", len);*/
//
//	//2. 要注意strlen的返回值类型是size_t
//	//3               - 6
//	//-3
//	//
//	if (strlen("abc") - strlen("abcdef") > 0)
//		printf(">\n");
//	else
//		printf("<=\n");
//
//	return 0;
//}


//strlen函数的模拟实现
//仿照strlen函数的参数，返回类型，功能写一个类似的函数
//

#include <assert.h>

//assert
//const
//指针解引用
//指针+整数
//
//size_t my_strlen(const char * str)
//{
//	size_t count = 0;//计数器，统计字符串的长度
//	assert(str != NULL);
//
//	while (*str != '\0')
//	{
//		count++;
//		str++;
//	}
//	return count;
//}

//指针-指针---> 地址-地址
//得到的指针和指针之间元素的个数
//
//size_t my_strlen(const char* str)
//{
//	const char* start = str;
//	while (*str != '\0')
//	{
//		str++;
//	}
//	return str - start;
//}

//
//方法3 - 递归的方式
//模拟实现strlen函数，不能使用临时变量
//

//递归的思想：大事化小
// 
//my_strlen("abcdef")
//1+my_strlen("bcdef")
//1+1+my_strlen("cdef")
//1+1+1+my_strlen("def")
//1+1+1+1+my_strlen("ef")
//1+1+1+1+1+my_strlen("f")
//1+1+1+1+1+1+my_strlen("")
//1+1+1+1+1+1+0 = 6
//


//size_t my_strlen(const char* str)
//{
//	if (*str != '\0')
//		return 1 + my_strlen(str + 1);
//	else
//		return 0;
//}

//
//int main()
//{
//	char arr[] = "abc";
//	size_t len = my_strlen(arr);
//	printf("%zd\n", len);
//
//	return 0;
//}

#include <string.h>

//
//strcpy
//函数的功能：拷贝字符串
//注意事项：
//1. 源字符串中必须包含\0，同时\0也会被拷贝到目标空间
//2. 程序员自己要保证目标空间要足够大，能放得下拷贝来的数据
//3. 保证目标空间必须可以修改
//
//int main()
//{
//	/*char arr1[] =   "hello abc";
//	char arr2[4] = "xxx";
//	strcpy(arr2, arr1);
//	printf("%s\n", arr2);*/
//
//	/*char arr1[4] = {'a', 'b', 'c', '\0'};
//	char arr2[10] = "xxx";
//	strcpy(arr2, arr1);
//	printf("%s\n", arr2);*/
//
//	char arr1[4] = "abc";
//	const char* p = "qwertyuiop";//常量字符串 - 不可以修改的
//
//	strcpy(p, arr1);
//
//	printf("%s\n", p);
//
//	return 0;
//}
//
//int main()
//{
//	const char* p = 2"abcdef";//常量字符串
//	*p = 'w';
//
//	return 0;
//}


//模拟实现strcpy函数  - 版本1
//void my_strcpy(char* dest, char* src)
//{
//	//拷贝\0	前的字符
//	while (*src != '\0')
//	{
//		*dest = *src;
//		dest++;
//		src++;
//	}
//	//拷贝\0
//	*dest = *src;
//}

//版本2
//void my_strcpy(char* dest, char* src)
//{
//	while (*dest++ = *src++)
//	{
//		;
//	}
//}

//版本3
//void my_strcpy(char* dest, char* src)
//{
//	//NULL
//	assert(dest);
//	assert(src);
//
//	while (*dest++ = *src++)
//	{
//		;
//	}
//}

//版本4
//dest指向的空间是需要改变的，但是src指向的空间是不期望被给变的
//
//void my_strcpy(char* dest, const char* src)
//{
//	//NULL
//	assert(dest);
//	assert(src);
//
//	while (*dest++ = *src++)
//	{
//		;
//	}
//}

//版本5
//char* my_strcpy(char* dest, const char* src)
//{
//	char* ret = dest;
//	//NULL
//	assert(dest);
//	assert(src);
//
//	while (*dest++ = *src++)
//	{
//		;
//	}
//
//	return ret;//目标空间的起始地址返回
//}
//
//
//int main()
//{
//	char arr1[] = "abcdef";
//	char arr2[20] = { 0 };
//	//链式访问
//	printf("%s\n", my_strcpy(arr2, arr1));
//
//	return 0;
//}
//

//strcat - 字符串追加
//1. 目标空间中得有\0(从哪里开始追加)，源头字符串中得有\0（追加到什么时候结束）
//2. 目标空间要足够大，目标要可以修改
//
//
//char* my_strcat(char* dest, const char* src)
//{
//	char* ret = dest;
//	assert(dest && src);
//	//1. 找到目标空间中的\0
//	while (*dest != '\0')
//	{
//		dest++;
//	}
//	//2. 拷贝数据
//	while (*dest++ = *src++)
//	{
//		;
//	}
//	return ret;
//}
//
//int main()
//{
//	char arr1[20] = "hello ";//hello world
//	char* p = "world";
//	printf("%s\n", my_strcat(arr1, p));
//
//	return 0;
//}




char* my_strcat(char* dest, const char* src)
{
	char* ret = dest;
	assert(dest && src);
	//1. 找到目标空间中的\0
	while (*dest != '\0')
	{
		dest++;
	}
	//2. 拷贝数据
	while (*dest++ = *src++)
	{
		;
	}
	return ret;
}

int main()
{
	char arr1[20] = "hello ";//hello world
	my_strcat(arr1, arr1);
	printf("%s\n", arr1);

	return 0;
}

//strncat



