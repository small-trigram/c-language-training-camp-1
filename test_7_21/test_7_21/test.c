#include <stdio.h>

//strlen - string length
#include <string.h>

//sizeof
//int main()
//{
//	int len = strlen("abc");//abc\0
//	printf("%d\n", len);
//	return 0;
//}

//int main()
//{
//	int len = strlen("c:\test\123.c");
//
//	printf("%d\n", len);
//
//	return 0;
//}

/*C语言的这种注释风格是不支持嵌套注释的*/

//int main()
//{
//	//int是C语言中的一种数据类型，这种数据类型是用来表示整数的
//	/*
//	int a = 0;
//	a = 2 + 3;
//	*/
//
//	printf("hehe\n");
//	
//	return 0;
//}

//int main()
//{
//	int num = 10;
//	return 0;
//}

#include <stdbool.h>

//int main()
//{
//	_Bool flag = true;
//	if (flag)
//		printf("I like C\n");
//	else
//		printf("hehe\n");
//	return 0;
//}

#include <limits.h>


//int main()
//{
//	int num;//
//	int Temperature1 = 30;
//	int Temperature2 = -30;
//
//	//unsigned int age = 20;
//	unsigned short age = INT_MIN;
//
//	return 0;
//}
//


//char short int long
//long long

//sizeof 是一个操作符
//计算的是变量所占内存的大小，单位是字节
//计算的是类型创建的变量所占内存的大小，单位是字节

//size_t - 是专门为sizeof 设置的一个类型
//size_t 的本质是unsigned int
//size_t 类型的数据在打印的时候，格式应该使用%zd

//int main()
//{
//	printf("%zd\n", sizeof(char));
//	printf("%zd\n", sizeof(short));
//	printf("%zd\n", sizeof(int));
//	printf("%zd\n", sizeof(long));
//	printf("%zd\n", sizeof(long long));
//	printf("%zd\n", sizeof(float));
//	printf("%zd\n", sizeof(double));
//
//	return 0;
//}
//



//
//int main()
//{
//	//int weight;
//	//char ch;
//	//float score;
//	//short age;
//
//	//初始化
//	int weight = 59;
//	char ch = 'q';
//	float score = 98.51f;
//	short age = 20;
//
//
//	return 0;
//}

//int g_val = 2023;//全局变量
//
//int main()
//{
//	int a = 0;
//	{
//		int b = 10;
//		printf("%d\n", b);
//		printf("g_val1 = %d\n", g_val);
//	}
//	printf("g_val2 = %d\n", g_val);
//
//	return 0;
//}
//

//int a = 10;
//int main()
//{
//	{
//		int a = 20;
//		printf("%d\n", a);
//	}
//	printf("%d\n", a);
//	return 0;
//}


//int main()
//{
//	int num1 = 3 + 7;
//	printf("%d\n", num1);
//	int num2 = 3 - 7;
//	printf("%d\n", num2);
//	          //10  -4
//	int sum = num1 + num2;
//	printf("%d\n", sum);
//	return 0;
//}

//
//int main()
//{
//	int n = 5;
//	printf("%d\n", n * n);
//
//	return 0;
//}
//

//int main()
//{
//	int a = 7 / 2;//7/2=3...1
//
//	printf("a = %d\n", a);
//	return 0;
//}

//int main()
//{
//	float a = 7.0f / 2.0f;//7/2=3...1
//
//	printf("a = %f\n", a);
//	return 0;
//}

//int main()
//{
//	int n = 5;
//	n = (n / 20.0) * 100;
//	printf("%d\n", n);
//	printf("n = %d\n", n);
//
//	return 0;
//}

//
//int main()
//{
//	int a = 7 / 2;//商
//	int b = 7 % 2;//%取模（取余）得到是是整除后的余数
//	printf("a=%d\n", a);
//	printf("b=%d\n", b);
//
//	return 0;
//}
//
//
//int main()
//{
//	int a = 0;//初始化
//	a = 20;//赋值，= 赋值操作符
//
//	return 0;
//}
//
//
//int main()
//{
//	int a = 3;
//	int b = 5;
//	int c = 0;
//	c = b = a + 3;//连续赋值，从右向左依次赋值的。
//	
//	b = a + 3;
//	c = b;
//
//	return 0;
//}





//+= -= *= /= %=
//
//int main()
//{
//	int a = 3;
//	a = a + 10;
//	a += 10;//复合赋值
//
//	a = a - 4;
//	a -= 4;//复合赋值
//
//	printf("%d\n", a);
//
//	return 0;
//}
//

//++ --> 自增1的运算

//int main()
//{
//	int a = 5;
//	//a = a + 1;
//	//a += 1;
//	//a++;
//	//++a;
//	printf("a=%d\n", a);
//
//	return 0;
//}

//前置和后置的区别是啥？
//放在表达式中才能观察

//前置++
//计算口诀：先++，后使用
//int main()
//{
//	int a = 5;
//	int b = ++a;
//  //a=a+1;b=a;
//	printf("a=%d\n", a);//6
//	printf("b=%d\n", b);//6
//
//
//	return 0;
//}

//后置++
//计算口诀：先使用，再++
//int main()
//{
//	int a = 5;
//	int b = a++;
//	//b=a;a=a+1;
//
//	printf("a=%d\n", a);//6
//	printf("b=%d\n", b);//5
//
//
//	return 0;
//}
//

//前置--
//计算口诀：先--，后使用
//int main()
//{
//	int a = 5;
//	int b = --a;
//	//a=a-1;b=a;
//	printf("a=%d\n", a);//4
//	printf("b=%d\n", b);//4
//
//	return 0;
//}


//后置--
//计算口诀：先使用，再--
//int main()
//{
//	int a = 5;
//	int b = a--;
//	//b=a, a=a-1
//	printf("a=%d\n", a);//4
//	printf("b=%d\n", b);//5
//
//	return 0;
//}
//


//int main()
//{
//	int a = -10;
//	int b = a;
//	printf("%d\n", b);
//
//	return 0;
//}
//
//


//int main()
//{
//	int a = (double)3.14;//double
//
//	printf("%d\n", a);
//	return 0;
//}
//
//手误，应该这样写：
//int main()
//{
//	int a = (int)3.14;
//
//	printf("%d\n", a);
//	return 0;
//}
//

//int main()
//{
//	int n = (int)3.14;
//	printf("%d\n", n);
//
//	return 0;
//}








