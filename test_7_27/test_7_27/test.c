#define _CRT_SECURE_NO_WARNINGS 1

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
//
//int main()
//{
//	//time_t;//int / long long
//	//RAND_MAX;
//	srand((unsigned int)time(NULL));
//
//	printf("%d\n", rand() % 100 + 1);
//	printf("%d\n", rand() % 100 + 1);
//	printf("%d\n", rand() % 100 + 1);
//	printf("%d\n", rand() % 100 + 1);
//	printf("%d\n", rand() % 100 + 1);
//
//	return 0;
//}


//
//
//void menu()
//{
//	printf("***********************\n");
//	printf("*****  1. play   ******\n");
//	printf("*****  0. exit   ******\n");
//	printf("***********************\n");
//}
//
//void game()
//{
//	int guess = 0;
//	//1. 生成随机数
//	int r = rand()%100+1;
//	//printf("%d\n", r);
//	//2. 猜数字
//	while (1)
//	{
//		printf("请猜数字:");
//		scanf("%d", &guess);
//		if (guess > r)
//		{
//			printf("猜大了\n");
//		}
//		else if (guess < r)
//		{
//			printf("猜小了\n");
//		}
//		else
//		{
//			printf("恭喜你，猜对了\n");
//			break;
//		}
//	}
//}
//
//int main()
//{
//	int input = 0;
//	srand((unsigned int)time(NULL));
//
//	do
//	{
//		//打印菜单
//		menu();
//
//		printf("请选择:>");
//		scanf("%d", &input);//1 0 8
//		switch (input)
//		{
//		case 1:
//			game();
//			break;
//		case 0:
//			printf("退出游戏\n");
//			break;
//		default:
//			printf("选择错误，重新选择\n");
//			break;
//		}
//
//	} while (input);
//
//	return 0;
//}


//
//
//void menu()
//{
//	printf("***********************\n");
//	printf("*****  1. play   ******\n");
//	printf("*****  0. exit   ******\n");
//	printf("***********************\n");
//}
//
//void game()
//{
//	int guess = 0;
//	//1. 生成随机数
//	int r = rand() % 100 + 1;
//	//printf("%d\n", r);
//	//2. 猜数字
//	int count = 5;
//	while (count)
//	{
//		printf("你还有%d次猜数字的机会\n", count);
//		printf("请猜数字:");
//		scanf("%d", &guess);
//		if (guess > r)
//		{
//			printf("猜大了\n");
//		}
//		else if (guess < r)
//		{
//			printf("猜小了\n");
//		}
//		else
//		{
//			printf("恭喜你，猜对了\n");
//			break;
//		}
//		count--;
//	}
//	if (count == 0)
//	{
//		printf("猜数字失败，正确的值是：%d\n", r);
//	}
//}
//
//int main()
//{
//	int input = 0;
//	srand((unsigned int)time(NULL));
//
//	do
//	{
//		//打印菜单
//		menu();
//
//		printf("请选择:>");
//		scanf("%d", &input);//1 0 8
//		switch (input)
//		{
//		case 1:
//			game();
//			break;
//		case 0:
//			printf("退出游戏\n");
//			break;
//		default:
//			printf("选择错误，重新选择\n");
//			break;
//		}
//
//	} while (input);
//
//	return 0;
//}
//


//int main()
//{
//	int math[20];
//
//	return 0;
//}


//int main()
//{
//	int num1[5] = {1,2,3,4,5};//完全初始化
//	int num2[5] = { 1,2,3 };//不完全初始化，剩余的元素初始化为0
//	int num3[] = { 1,2,3 };//如果数组初始化了，是可以省略掉数组大小的
//	//数组的大小，是编译器根据初始化的内容确实的
//
//	int num4[5] = { 1,2,3,4,5 };
//
//	return 0;
//}

//int main()
//{
//	int num1[5] = { 1,2,3,4,5 };//num1的类型是int [5]
//	int num2[10] = { 1,2,3 };//num2的类型是int [10]
//
//	char ch[8] = {'a', 'b', 'c'};
//
//	return 0;
//}


//int main()
//{
//	int num1[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	//printf("%d\n", num1[6]);//[] 下标引用操作符
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", num1[i]);
//	}
//	return 0;
//}


//C99 标准之前数组的大小只能是常量指定，不能使用变量
//C99 之后为什么就使用了变量呢？--- 变长数组

//int main()
//{
//	int arr[10] = {0};
//	int n = 10;
//	int arr2[n];//err
//	return 0;
//}

//
//int main()
//{
//	int num1[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	//printf("%d\n", num1[6]);//[] 下标引用操作符
//	int i = 0;
//
//	//输入
//	for (i = 0; i < 10; i++)//0~9
//	{
//		scanf("%d", &num1[i]);
//	}
//	//输出
//	for (i = 0; i < 10; i++)//0~9
//	{
//		printf("%d ", num1[i]);
//	}
//	return 0;
//}
// 
// 
//%p - 打印地址
//

//int main()
//{
//	int num1[15] = { 1,2,3,4,5,6,7,8,9,10 };
//	int i = 0;
//
//	for (i = 0; i < 10; i++)//0~9
//	{
//		printf("&arr[%d] = %p\n", i,  &num1[i]);
//	}
//	return 0;
//}


//int main()
//{
//	int arr[15] = { 1,2,3 };//10*4 = 40
//	printf("%d\n", sizeof(arr));//sizeof(数组名)计算的是数组所占内存空间的大小，单位是字节
//	printf("%d\n", sizeof(arr[0]));
//	printf("%d\n", sizeof(arr) / sizeof(arr[0]));//计算的是数组的元素个数10
//
//	int sz = sizeof(arr) / sizeof(arr[0]);
//
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//
//	return 0;
//}

//
//sizeof 的计算机结果是size_t 类型的
//size_t 是无符号的整型
//
//int main()
//{
//	int a = 10;
//	//size_t;
//	printf("%zd\n", sizeof(int));
//	printf("%zd\n", sizeof(int));
//	printf("%zd\n", sizeof(a));
//	printf("%zd\n", sizeof a);
//
//	return 0;
//}

//
//talk is cheap, show me the code
//


//
//int main()
//{
//	//double score[20][5];
//	//不完全初始化
//	int arr1[3][5] = { 1,2 };
//	int arr2[3][5] = {0};
//	int arr3[3][5] = { 1,2,3,4,5,6,7 };
//	int arr4[3][5] = { {1,2},{3,4,5},{6,7} };
//
//
//
//	return 0;
//}
//

//int main()
//{
//	int arr5[][5] = { 1,2,3 };
//	int arr6[][5] = { 1,2,3,4,5,6,7 };
//	int arr7[][5] = { {1,2}, {3,4}, {5,6} };
//	return 0;
//}
//
//
//int main()
//{
//	int arr[3][5] = { {1,2,3,4,5},{2,3,4,5,6}, {3,4,5,6,7} };
//	//printf("%d\n", arr[1][3]);//[]下标引用操作符
//	int i = 0;
//	//输入
//	for (i = 0; i < 3; i++)
//	{
//		//i
//		int j = 0;
//		for (j = 0; j < 5; j++)
//		{
//			scanf("%d", &arr[i][j]);
//		}
//	}
//
//	
//	//输出
//	for (i = 0; i < 3; i++)
//	{
//		//i
//		int j = 0;
//		for (j = 0; j < 5; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//
//	return 0;
//}


//
//int main()
//{
//	int arr[3][5] = { {1,2,3,4,5},{2,3,4,5,6}, {3,4,5,6,7} };
//	int i = 0;
//
//
//	//输出
//	for (i = 0; i < 3; i++)
//	{
//		//i
//		int j = 0;
//		for (j = 0; j < 5; j++)
//		{
//			printf("&arr[%d][%d] = %p\n", i, j, &arr[i][j]);
//		}
//	}
//
//	return 0;
//}

//C99 之前创建的数组的方式，数组的大小是使用常量、常量表达式指定的
//int main()
//{
//	int arr1[10];
//	int arr2[3 + 5];
//	int arr3[] = { 1,2,3 };
//
//	return 0;
//}
//C99中，引入了变长数组的概念，允许数组的大小是变量的
//VS虽然支持了C99的语法，但是不是全部支持的
//比如：变长数组在VS上是不支持的
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);//10
//	int arr[n];
//	return 0;
//}
//gcc 编译器上给大家做个验证

//练习1：多个字符从两端移动，向中间汇聚
//welcome to bit!!!!!!!
//*********************
//w*******************!
//we*****************!!
//wel***************!!!
//...
//welcome to bit!!!!!!!
#include <string.h>
#include <windows.h>

int main()
{
	char arr1[] = "welcome to bit!!!!!!!";
	char arr2[] = "*********************";

	int left = 0;
	int right = strlen(arr1)-1;

	while (left<=right)
	{
		arr2[left] = arr1[left];
		arr2[right] = arr1[right];
		printf("%s\n", arr2);
		Sleep(1000);//休眠，单位是毫秒
		system("cls");//用来执行系统命令的
		left++;
		right--;
	}

	printf("%s\n", arr2);

	//char arr[] = "abc";
	//printf("%d\n", strlen(arr));

	return 0;
}










