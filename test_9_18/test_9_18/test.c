#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <stdlib.h>
//
//int main()
//{
//	//假设申请10个整型的空间
//	int* p = (int*)malloc(10*sizeof(int));
//	if (p == NULL)
//	{
//		perror("malloc");
//		return 1;
//	}
//	//使用
//	int i = 0;
//
//	//0 1 2 3 4 5 6 7 8 9
//	for (i = 0; i < 10; i++)
//	{
//		*(p + i) = i;
//	}
//	//打印
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", p[i]);
//	}
//	//释放内存空间
//	free(p);
//	p = NULL;
//
//	return 0;
//}

//malloc在堆区上申请内存空间
//如果需要释放空间，需要使用free函数
//如果没有使用free释放，在程序退出的时候，也会由操作系统来回收
//
//int main()
//{
//	//假设申请10个整型的空间
//	int* p = (int*)malloc(INT_MAX*4);//开辟失败的
//	if (p == NULL)
//	{
//		perror("malloc");
//		return 1;
//	}
//	//使用
//	int i = 0;
//
//	//0 1 2 3 4 5 6 7 8 9
//	for (i = 0; i < 10; i++)
//	{
//		*(p + i) = i;
//	}
//	//打印
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", p[i]);
//	}
//	
//	return 0;
//}
//
//int main()
//{
//	//calloc申请的空间默认会被初始化为0
//	//malloc申请的空间不会初始化
//
//	//int *p = (int*)calloc(10, sizeof(int));
//	int* p = (int*)malloc(10*sizeof(int));
//	if (p == NULL)
//	{
//		perror("calloc");
//		return 1;
//	}
//	//使用内存
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d\n", p[i]);
//	}
//	//释放
//	free(p);
//	p = NULL;
//
//	return 0;
//}
//

//
//int main()
//{
//	int*p = (int*)malloc(5*sizeof(int));
//	if (p == NULL)
//	{
//		perror("malloc");
//		return 1;
//	}
//	//使用
//	int i = 0;
//	for (i = 0; i < 5; i++)
//	{
//		*(p + i) = i + 1;
//	}
//	/*for (i = 0; i < 5; i++)
//	{
//		printf("%d ", *(p+i));
//	}*/
//	//调整申请的堆上内存
//	int* ptr = (int*)realloc(p, 40);
//	if (ptr != NULL)
//	{
//		p = ptr;
//		ptr = NULL;
//	}
//	else
//	{
//		perror("realloc");
//		return 1;
//	}
//
//	//使用
//	for (i = 5; i < 10; i++)
//	{
//		*(p + i) = i+1;
//	}
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d\n", *(p + i));
//	}
//
//	//释放
//	free(p);
//	p = NULL;
//
//	return 0;
//}


//int main()
//{
//	//当realloc函数的第一个参数是NULL指针的时候，功能类似于malloc函数
//
//	int*p = (int*)realloc(NULL, 40);//malloc(40)
//
//	return 0;
//}

//4.1 对NULL指针的解引用操作
//int main()
//{
//	int*p = (int*)malloc(20);
//	if (p == NULL)
//	{
//		return 1;
//	}
//	int i = 0;
//	for (i = 0; i < 5; i++)
//	{
//		*(p + i) = i;
//	}
//
//	return 0;
//}

//4.2 对动态开辟空间的越界访问
//
//int main()
//{
//	int* p = (int*)malloc(20);//20个字节 - 5个整型
//	if (p == NULL)
//	{
//		return 1;
//	}
//	int i = 0;
//	//越界访问
//	for (i = 0; i < 20; i++)
//	{
//		*(p + i) = i;
//	}
//	free(p);
//	p = NULL;
//
//	return 0;
//}

//4.3 对非动态开辟内存使用free释放
//int main()
//{
//	int a = 10;
//	int* p = &a;
//	//...
//
//	free(p);
//	p = NULL;
//
//	return 0;
//}

//4.4 使用free释放一块动态开辟内存的一部分 - err

//int main()
//{
//	int* p = (int*)malloc(40);
//	if (p == NULL)
//	{
//		perror("malloc");
//		return 1;
//	}
//	//开辟成功
//	int i = 0;
//
//	//1 2 3 4 5 [] [] [] [] []
//	for (i = 0; i < 5; i++)
//	{
//		*p = i + 1;
//		p++;
//	}
//	//释放
//	free(p);
//	p = NULL;
//
//	return 0;
//}
//

//4.5 对同一块动态内存多次释放

//int main()
//{
//	int* p = (int*)malloc(40);
//	if (p == NULL)
//	{
//		perror("malloc");
//		return 1;
//	}
//	//...
//
//	//释放
//	free(p);
//	//....
//
//	free(p);
//
//	return 0;
//}

//4.6 动态开辟内存忘记释放（内存泄漏）

//void test()
//{
//	int* p = (int*)malloc(40);
//	if (p == NULL)
//	{
//		return;
//	}
//	//使用
//	//....
//
//	if (5 == 2 + 3)
//	{
//		return;
//	}
//
//	//释放
//	free(p);
//	p = NULL;
//}
//
//int main()
//{
//	test();
//	//程序不退出
//	//7*24一直在跑
//
//	return 0;
//}
#include <string.h>

//void GetMemory(char** p)
//{
//    *p = (char*)malloc(100);
//}
//
//void Test(void)
//{
//    char* str = NULL;
//    GetMemory(&str);
//    strcpy(str, "hello world");
//    printf(str);
//    free(str);
//    str = NULL;
//}
//
//int main()
//{
//    Test();
//    return 0;
//}

//char* GetMemory()
//{
//    char* p = (char*)malloc(100);
//    return p;
//}
//
//void Test(void)
//{
//    char* str = NULL;
//    str = GetMemory();
//    strcpy(str, "hello world");
//    printf(str);
//    free(str);
//    str = NULL;
//}
//
//int main()
//{
//    Test();
//    return 0;
//}
//

//char* GetMemory(void)
//{
//    char p[] = "hello world";
//    return p;
//}
//void Test(void)
//{
//    char* str = NULL;
//    str = GetMemory();
//    printf(str);
//}
//
//int main()
//{
//    Test();
//	return 0;
//}

//int* test()
//{
//	int a = 10;
//	return &a;
//}
//
//int main()
//{
//	int * p = test();
//	//p就是野指针了
//	//printf("hehe\n");
//
//	printf("%d\n", *p);
//
//	return 0;
//}

//void GetMemory(char** p, int num)
//{
//    *p = (char*)malloc(num);
//}
//
//void Test(void)
//{
//    char* str = NULL;
//    GetMemory(&str, 100);
//    strcpy(str, "hello");
//    printf(str);
//    free(str);
//    str = NULL;
//}
//
//int main()
//{
//    Test();
//    return 0;
//}




//void Test(void)
//{
//    char* str = (char*)malloc(100);
//    strcpy(str, "hello");
//    free(str);//
//    str = NULL;
//
//    //str 就是野指针
//    if (str != NULL)
//    {
//        strcpy(str, "world");//非法访问
//        printf(str);
//    }
//}
//
//
//int main()
//{
//    Test();
//
//    return 0;
//}


//
//struct st_type
//{
//    int i;
//    int a[0];//柔性数组成员
//};
//
//
//struct st_type
//{
//    int i;
//    int a[];//柔性数组成员
//};
//


//struct S
//{
//	int a;
//	char c;
//	int arr[];
//};
// 
// 

//struct st_type
//{
//    int i;
//    int a[10];
//};

//
//struct st_type
//{
//    int i;
//    int a[0];//柔性数组成员
//};
//
//int main()
//{
//    //printf("%d\n", sizeof(struct st_type));
//    struct st_type* p = (struct st_type*)malloc(sizeof(struct st_type) + 10*sizeof(int));
//    if (p == NULL)
//    {
//        perror("malloc");
//        return 1;
//    }
//    //使用
//    int i = 0;
//    p->i = 100;
//
//    for (i = 0; i < 10; i++)
//    {
//        p->a[i] = i;
//    }
//    //我们希望结构中的a数组变长为60个字节
//    struct st_type* ptr = (struct st_type*)realloc(p, sizeof(struct st_type)+15*sizeof(int));
//    if (ptr != NULL)
//    {
//        p = ptr;
//        ptr = NULL;
//    }
//    else
//    {
//        perror("realloc");
//        return 1;
//    }
//    //使用
//    //...
//    
//    //释放
//    free(p);
//    p = NULL;
//
//	return 0;
//}
//



struct st_type
{
    int i;
    int* a;
};

int main()
{
    struct st_type* p = (struct st_type*)malloc(sizeof(struct st_type));
    if (p == NULL)
    {
        perror("malloc\n");
        return 1;
    }
    p->i = 100;
    p->a = malloc(10*sizeof(int));
    if (p->a == NULL)
    {
        perror("malloc");
        return 1;
    }
    int i = 0;
    for (i = 0; i < 10; i++)
    {
        p->a[i] = i;
    }

    //希望a指向的空间是15 个整型
    //struct st_type* ptr = (struct st_type*)realloc(p->a, 15 * sizeof(int));//错误的
    //应该改为下面代码：
    int* ptr = (int*)realloc(p->a, 15 * sizeof(int));
    if (ptr == NULL)
    {
        perror("realloc");
        return 1;
    }
    else
    {
        //p = ptr;//错误的
        //应该改为：
        p->a = ptr;
        ptr = NULL;
    }
    //使用


    //释放
    free(p->a);
    p->a = NULL;
    free(p);
    p = NULL;

    return 0;
}


